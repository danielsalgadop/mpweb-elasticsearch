<?php require __DIR__ . '/vendor/autoload.php';

use App\Search\SearchBankAccounts;
use App\Search\SearchExactMatch;
use App\Search\SearchFuzzyMatch;
use App\Search\SearchQuery;
use Elasticsearch\ClientBuilder;

$name    = (isset($_GET['name'])) ? $_GET['name'] : '';
$surname = isset($_GET['surname']) ? $_GET['surname'] : '';

$matchType = new SearchExactMatch();
//$matchType = new SearchFuzzyMatch();
$searchQuery = new SearchQuery($matchType);
$searchQuery->byName($name)->bySurname($surname);

$client = ClientBuilder::create()->build();
$searchBankAccounts = new SearchBankAccounts($client, $searchQuery);
$filterResults      = $searchBankAccounts->search();

?>

<div class="box">
	<div class="box-header">
		<h3 class="box-title">Results (hits=<?= $total_hits = $filterResults['hits']['total'] ?>)</h3>

		<div class="box-tools">
			<div class="input-group input-group-sm" style="width: 150px;">
			</div>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body table-responsive">
		<table class="table table-hover table-striped">
			<tbody>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Surname</th>
				<th>Balance</th>
				<th>Email</th>
			</tr>
			<?php

			foreach ($filterResults['hits']['hits'] as $filterResult) {
				$result = $filterResult['_source'];
				?>
				<tr>
					<td>#<?= $result['account_number'] ?></td>
					<td><?= $result['firstname'] ?></td>
					<td><?= $result['lastname'] ?></td>
					<td>$ <?= $result['balance'] ?></td>
					<td><?= $result['email'] ?></td>
				</tr>
				<?php
			}
			?>
			</tbody>


		</table>
		<hr>
		<?php
		echo "<pre>ElasticSearch QUERY \n";
		echo json_encode($searchQuery->getFilter(), JSON_PRETTY_PRINT);
		echo "</pre>";
		?>
	</div>
	<!-- /.box-body -->
</div>
<!-- /.box -->